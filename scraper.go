package main

import (
	"crypto/tls"
	"net/http"
	"net/url"
	"sync"
	"time"

	log "github.com/sirupsen/logrus"

	"github.com/gocolly/colly"
	"github.com/gocolly/colly/extensions"
)

const (
	TIME_START = "start"
)

type PageResponse struct {
	URL        string
	StatusCode int
	Data       []byte
	Depth      int
	Duration   time.Duration
	Headers    *http.Header
}

type Scraper struct {
	ID           int32
	MaxDepth     int
	website      string
	Recursively  bool
	WithExternal bool
	Async        bool
	IgnoreSSL    bool
	waitGroup    sync.WaitGroup
	stats        map[string]*PageStats
	mutex        *sync.Mutex
	Hostname     string
	Indexability string
}

func (s *Scraper) setWebsite(website string) {
	u, err := url.ParseRequestURI(website)
	if err == nil {
		s.Hostname = u.Hostname()
		s.website = website
	} else {
		log.WithError(err).WithField("url", website).Error("Failed to parse URL")
		s.Hostname = website
		s.website = website
	}
}

func (s *Scraper) handleResponse(r *colly.Response) {
	log.WithField("url", r.Request.URL.String()).Trace("Received response")
	p := &PageResponse{
		URL:        r.Request.URL.String(),
		Data:       r.Body,
		StatusCode: r.StatusCode,
		Depth:      r.Request.Depth,
		Headers:    r.Headers,
	}
	start := r.Ctx.GetAny(TIME_START)
	if start != nil {
		p.Duration = time.Since(start.(time.Time))
	}

	s.waitGroup.Add(1)
	go s.processPage(p)
}

func (s *Scraper) Scrape(website string) error {
	s.setWebsite(website)
	log.WithField("url", website).Debug("About to scrape website")

	//c := colly.NewCollector(colly.Debugger(&debug.LogDebugger{}))
	c := colly.NewCollector()
	if s.IgnoreSSL {
		c.WithTransport(&http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		})
	}
	extensions.RandomUserAgent(c)

	c.MaxDepth = s.MaxDepth
	c.Async = s.Async

	// external collector will not process the whole external website, just the first link
	externalCollector := c.Clone()
	externalCollector.MaxDepth = 1

	c.OnRequest(func(r *colly.Request) {
		r.Ctx.Put(TIME_START, time.Now())
	})

	c.OnResponse(func(r *colly.Response) {
		s.handleResponse(r)
	})

	c.OnError(func(r *colly.Response, err error) {
		log.WithError(err).WithField("url", r.Request.URL.String()).Error("Failed to fetch URL")
		s.handleResponse(r)
	})

	externalCollector.OnRequest(func(r *colly.Request) {
		r.Ctx.Put(TIME_START, time.Now())
	})

	externalCollector.OnResponse(func(r *colly.Response) {
		s.handleResponse(r)
	})

	externalCollector.OnError(func(r *colly.Response, err error) {
		log.WithError(err).WithField("url", r.Request.URL.String()).Error("Failed to fetch external URL")
		s.handleResponse(r)
	})

	// Find and visit all links
	c.OnHTML("a[href]", func(e *colly.HTMLElement) {
		link := e.Request.AbsoluteURL(e.Attr("href"))

		u, err := url.ParseRequestURI(link)
		if err != nil {
			return
		}

		log.WithField("url", link).Debug("Visiting page")

		if u.Hostname() == s.Hostname {
			err = e.Request.Visit(link)
		} else if s.WithExternal {
			log.WithField("link", link).Trace("Using external collector")
			err = externalCollector.Visit(link)
		}

		if err != nil {
			if err != colly.ErrAlreadyVisited {
				log.WithError(err).Error("Failed to visit link")
			}
		}
	})

	// Start the scrape
	if err := c.Visit(s.website); err != nil {
		log.WithError(err).WithField("website", s.website).Error("Failed to visit website")
	}

	log.Debug("Waiting for the scape to finish")
	c.Wait()
	externalCollector.Wait()

	log.Debug("Waiting for the page processing to finish...")
	s.waitGroup.Wait()

	return nil
}

func (s *Scraper) include(p *PageStats) bool {
	switch s.Indexability {
	case "ok":
		return p.Indexibility == INDEXABLE
	case "non":
		return p.Indexibility == NON_INDEXABLE
	}
	return true
}

func (s *Scraper) Report() []*PageStats {
	log.WithField("count", len(s.stats)).Debug("Reporting stats")
	result := make([]*PageStats, 0, len(s.stats))
	for _, v := range s.stats {
		if s.include(v) {
			result = append(result, v)
		}
	}
	return result
}
