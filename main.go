package main

import (
	"bufio"
	"flag"
	"os"
	"runtime"
	"sync"
	"sync/atomic"

	log "github.com/sirupsen/logrus"
)

// flags
var (
	maxDepthFlag     = flag.Int("depth", 1, "Maximum depth for crawling")
	verboseFlag      = flag.Bool("verbose", false, "Write verbose logs")
	formatFlag       = flag.String("format", "table", "Format of the output (table|csv|tsv)")
	indexFlag        = flag.String("index", "", "Indexability (ok | non)")
	shortFlag        = flag.Bool("short", false, "Print less output per URL")
	ignoreCertFlag   = flag.Bool("ignore-cert", false, "Ignore expired certificates")
	logFileFlag      = flag.String("log", "mink.log", "Path to the logfile")
	withExternalFlag = flag.Bool("external", false, "Include external links")
)

var (
	id int32
)

func NewReporter(short bool) Reporter {
	switch *formatFlag {
	case "table":
		return NewTableReporter(short)
	case "csv":
		return NewCSVReporter(short)
	case "tsv":
		return NewTSVReporter(short)
	default:
		return NewTableReporter(short)
	}
}

func main() {
	log.SetFormatter(&log.JSONFormatter{})

	flag.Parse()

	if *verboseFlag {
		log.SetLevel(log.TraceLevel)
	} else {
		log.SetLevel(log.DebugLevel)
	}

	file, err := os.OpenFile(*logFileFlag, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	if err == nil {
		log.SetOutput(file)
		defer file.Close()
	}
	scanner := bufio.NewScanner(os.Stdin)

	var wg sync.WaitGroup
	sem := make(chan struct{}, concurrencyNumber())
	reporter := NewReporter(*shortFlag)
	reports := make(chan *PageStats)

	go func() {
		for r := range reports {
			if err := reporter.Append(r); err != nil {
				log.WithError(err).Error("Failed to append report")
			}
			wg.Done()
		}
	}()

	visited := make(map[string]bool)

	for scanner.Scan() {
		next := scanner.Text()
		if _, ok := visited[next]; ok {
			continue
		}
		visited[next] = true
		wg.Add(1)
		sem <- struct{}{}
		go func(url string, rc chan *PageStats) {
			defer wg.Done()
			processUrl(url, rc, &wg)
			<-sem
		}(next, reports)
	}
	wg.Wait()
	close(reports)

	_ = reporter.Render()
}

func concurrencyNumber() int {
	count := runtime.NumCPU() / *maxDepthFlag
	if count < 1 {
		count = 1
	}
	return count
}

func processUrl(url string, reports chan *PageStats, wg *sync.WaitGroup) {
	s := &Scraper{
		ID:           atomic.AddInt32(&id, 1),
		MaxDepth:     *maxDepthFlag,
		IgnoreSSL:    *ignoreCertFlag,
		WithExternal: *withExternalFlag,
		Async:        true,
		mutex:        &sync.Mutex{},
		stats:        make(map[string]*PageStats),
		Indexability: *indexFlag,
	}
	_ = s.Scrape(url)
	for _, r := range s.Report() {
		wg.Add(1)
		reports <- r
	}
}
