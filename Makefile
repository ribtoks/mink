build:
	env GOFLAGS="-mod=vendor" go build

vendors:
	go mod tidy
	go mod vendor
