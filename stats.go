package main

import (
	"bytes"
	"net/http"
	"net/url"
	"strings"
	"time"
	"unicode"

	log "github.com/sirupsen/logrus"

	"github.com/PuerkitoBio/goquery"
	"github.com/microcosm-cc/bluemonday"
)

const (
	INDEXABLE     = "Indexable"
	NON_INDEXABLE = "Non-Indexable"
	NOINDEX       = "noindex"
)

type PageStats struct {
	URL                   string
	Domain                string
	StatusCode            int
	Status                string
	Indexibility          string
	ContentType           string
	Title                 string
	TitleLength           int
	MetaDescription       string
	MetaDescriptionLength int
	MetaKeywords          string
	MetaKeywordsCount     int
	Size                  int
	WordCount             int
	CrawlDepth            int
	Inlinks               int
	UniqueInlinks         int
	Outlinks              int
	UniqueOutlinks        int
	ResponseTimeMillis    int
	Emails                string
}

func stripHtml(data []byte) string {
	stripped := bluemonday.StrictPolicy().SanitizeBytes(data)
	return string(stripped)
}

func countWords(s string) int {
	nWords := 0
	inWord := false
	for _, r := range s {
		if unicode.IsSpace(r) || unicode.IsPunct(r) {
			inWord = false
		} else if !inWord {
			inWord = true
			nWords++
		}
	}
	return nWords
}

func isRedirect(hostname string, document *goquery.Document) bool {
	hasCanonicalLink := false
	document.Find("link").Each(func(i int, s *goquery.Selection) {
		if rel, _ := s.Attr("rel"); rel == "canonical" {
			if href, exists := s.Attr("href"); exists {
				if u, err := url.ParseRequestURI(href); err == nil {
					hasCanonicalLink = len(u.Hostname()) > 0 && hostname != u.Hostname()
				}
			}
		}
	})
	return hasCanonicalLink
}

func isNoIndex(document *goquery.Document) bool {
	hasNoIndex := false
	document.Find("meta").Each(func(i int, s *goquery.Selection) {
		if name, _ := s.Attr("name"); name == "robots" {
			if content, exists := s.Attr("content"); exists {
				hasNoIndex = strings.Contains(content, NOINDEX)
			}
		}
	})
	return hasNoIndex
}

func (p *PageResponse) isIndexable(hostname string, document *goquery.Document) bool {
	if p.StatusCode/100 != 2 {
		return false
	}

	robots := p.Headers.Get("X-Robots-Tag")
	if strings.Contains(robots, NOINDEX) {
		return false
	}

	if isNoIndex(document) {
		return false
	}

	if isRedirect(hostname, document) {
		return false
	}

	return true
}

func (p *PageResponse) indexibility(hostname string, document *goquery.Document) string {
	if p.isIndexable(hostname, document) {
		return INDEXABLE
	}

	return NON_INDEXABLE
}

func extractLinks(document *goquery.Document) map[string]int {
	links := make(map[string]int)
	document.Find("a").Each(func(index int, element *goquery.Selection) {
		// See if the href attribute exists on the element
		href, exists := element.Attr("href")
		if !exists {
			return
		}
		href = strings.ToLower(href)
		_, ok := links[href]
		if ok {
			links[href] += 1
		} else {
			links[href] = 1
		}
	})
	return links
}

func (ps *PageStats) countLinks(hostname string, links map[string]int) {
	for k, v := range links {
		l, err := url.ParseRequestURI(k)
		if err != nil {
			continue
		}
		if l.IsAbs() && l.Hostname() != hostname {
			ps.Outlinks += v
			ps.UniqueOutlinks += 1
		} else {
			ps.Inlinks += v
			ps.UniqueInlinks += 1
		}
	}
}

func extractMetaDescription(document *goquery.Document) string {
	description := ""
	document.Find("meta").Each(func(i int, s *goquery.Selection) {
		if name, _ := s.Attr("name"); strings.ToLower(name) == "description" {
			description, _ = s.Attr("content")
		}
	})
	return description
}

func extractMetaKeywords(document *goquery.Document) (string, int) {
	keywords := ""
	document.Find("meta").Each(func(i int, s *goquery.Selection) {
		if name, _ := s.Attr("name"); strings.ToLower(name) == "keywords" {
			keywords, _ = s.Attr("content")
		}
	})
	if len(keywords) == 0 {
		return "", 0
	}
	arr := strings.Split(keywords, ",")
	return keywords, len(arr)
}

func (s *Scraper) processPage(p *PageResponse) {
	log.WithFields(log.Fields{"url": p.URL, "bytes": len(p.Data)}).Debug("Processing page")
	defer s.waitGroup.Done()
	u, err := url.ParseRequestURI(p.URL)
	if err != nil {
		log.WithError(err).Error("Error parsing request URL")
		return
	}
	ps := &PageStats{}

	buf := bytes.NewBuffer(p.Data)
	if document, err := goquery.NewDocumentFromReader(buf); err == nil {
		ps.Indexibility = p.indexibility(u.Hostname(), document)
		ps.Title = document.Find("title").Text()
		ps.MetaDescription = extractMetaDescription(document)
		ps.MetaDescriptionLength = len(ps.MetaDescription)
		keywords, keywordsCount := extractMetaKeywords(document)
		ps.MetaKeywords = keywords
		ps.MetaKeywordsCount = keywordsCount
		links := extractLinks(document)
		log.WithField("links", links).Debug("Parsed page links")
		ps.countLinks(strings.ToLower(u.Hostname()), links)
	} else {
		log.WithError(err).Error("Error loading HTTP response body")
	}

	ps.URL = p.URL

	ps.Domain = u.Hostname()
	ps.StatusCode = p.StatusCode
	ps.Status = http.StatusText(p.StatusCode)
	if p.Headers != nil {
		ps.ContentType = p.Headers.Get("Content-Type")
	}
	ps.TitleLength = len(ps.Title)
	if len(p.Data) > 0 {
		ps.Size = len(p.Data)
		ps.WordCount = countWords(stripHtml(p.Data))
		ps.Emails = strings.Join(parseEmails(p.Data), ";")
	}
	ps.CrawlDepth = p.Depth
	ps.ResponseTimeMillis = int(p.Duration / time.Millisecond)

	s.mutex.Lock()
	defer s.mutex.Unlock()
	s.stats[p.URL] = ps
}
